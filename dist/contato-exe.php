<?php
// Carrega PHPMailer
require_once("PHPMailer/PHPMailerAutoload.php");

// Armazena os dados em variáveis escapando-os
$contato    = filter_input(INPUT_POST, 'contato', FILTER_SANITIZE_STRING);
$newsletter = filter_input(INPUT_POST, 'newsletter', FILTER_SANITIZE_STRING);

if($contato == 'contato')
{
	$nome  		= filter_input(INPUT_POST, 'nome', FILTER_SANITIZE_STRING);
	$email 		= filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
	$telefone 	= filter_input(INPUT_POST, 'fone', FILTER_SANITIZE_STRING);
	$mensagem 	= filter_input(INPUT_POST, 'msg', FILTER_SANITIZE_STRING);

	// Monta o html de envio de e-mail
	$strMensagem  = "<html>";
	$strMensagem .= "  <head><meta charset='utf-8'><title>Frigorífico Torres</title></head>";
	$strMensagem .= "  <body>";
	$strMensagem .= "    <table>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td colspan='2'><h1 style='font-family: Arial;'>Mensagem de cliente pelo site do Frigorifico Torres</h1></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Nome:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $nome . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>E-mail:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $email . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Telefone:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $telefone . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>Mensagem:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $mensagem . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "    </table>";
	$strMensagem .= "  </body>";
	$strMensagem .= "</html>";

	$mail = new PHPMailer;
	$mail->CharSet = "UTF-8";
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = "smtp.gmail.com";
	$mail->Port = 465;
	$mail->SMTPSecure = 'ssl';
	//$mail->SMTPOptions = array(
	//	'ssl' => array(
//			'verify_peer' => false,//
//			'verify_peer_name' => false,
	//		'allow_self_signed' => true
	//	)
//	);

	$mail->SMTPAuth = true;
	$mail->Username = "contato@frigorificotorres.com.br";
	$mail->Password = "qwerty!!";
	$mail->setFrom('contato@frigorificotorres.com.br', 'Contato do Site - Nome: '.$nome);
	$mail->addReplyTo('contato@frigorificotorres.com.br', 'Contato do Site: '.$nome);
	$mail->addAddress('contato@frigorificotorres.com.br', 'Atendimento Frigorífico Torres');
}

else if($newsletter == 'news')
{
	$email 		= filter_input(INPUT_POST, 'news-email', FILTER_SANITIZE_EMAIL);

	// Monta o html de envio de e-mail
	$strMensagem  = "<html>";
	$strMensagem .= "  <head><meta charset='utf-8'><title>Frigorífico Torres</title></head>";
	$strMensagem .= "  <body>";
	$strMensagem .= "    <table>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td colspan='2'><h1 style='font-family: Arial;'>Nova Solicitação de Newsletter</h1></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "      <tr>";
	$strMensagem .= "        <td><font face='Arial' size='2'>E-mail:</font></td>";
	$strMensagem .= "        <td><font face='Arial' size='2'>" . $email . "</font></td>";
	$strMensagem .= "      </tr>";
	$strMensagem .= "    </table>";
	$strMensagem .= "  </body>";
	$strMensagem .= "</html>";

	$mail = new PHPMailer;
	$mail->CharSet = "UTF-8";
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = "smtp.gmail.com";
	$mail->Port = 465;
	$mail->SMTPSecure = 'ssl';
	//$mail->SMTPOptions = array(
	//	'ssl' => array(
//			'verify_peer' => false,//
//			'verify_peer_name' => false,
	//		'allow_self_signed' => true
	//	)
//	);

	$mail->SMTPAuth = true;
	$mail->Username = "contato@frigorificotorres.com.br";
	$mail->Password = "qwerty!!";
	$mail->setFrom('contato@frigorificotorres.com.br', 'Newsletter');
	$mail->addReplyTo('contato@frigorificotorres.com.br', 'Contato do Site: '.$email);
	$mail->addAddress('contato@frigorificotorres.com.br', 'Atendimento Frigorífico Torres');
}

$mail->Subject = 'Site Frigorifico Torres - Mensagem de Visitante';
$mail->msgHTML($strMensagem);

if (!$mail->send()) {
	$retorno = array(
		'code' => 0,
	);

	echo json_encode($retorno);
} else {
	$retorno = array(
		'code' => 1,
	);

	echo json_encode($retorno);
}