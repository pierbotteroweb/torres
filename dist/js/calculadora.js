
if(enderecoArrayUltimo==""||enderecoArrayUltimo=="?calculadora"){

var listaChurrasco = document.getElementsByClassName("lista-calc")[0];
var seletorChurrasco = document.getElementsByTagName("select")[0];
seletorChurrasco.addEventListener('change',function(){

	var churrasco = {
		"Picanha": 100,
		"Bife Ancho": 100,
		"Linguiça Calabresa": 130,
		"Linguiça Toscana": 100,
		"Linguiça de Frango": 50,
		"Espeto de Lombo": 120,
		"Espeto de Contrqa Filé": 120

	}

	var stringChurrasco = "";
	var indice = seletorChurrasco.selectedIndex+1;

	for (item in churrasco){
		stringChurrasco = stringChurrasco + "<li>"+(indice*churrasco[item]).toFixed(0)+"g de "+item+"</li>"
	}
	listaChurrasco.innerHTML=stringChurrasco;


})

var listaFeijoada = document.getElementsByClassName("lista-calc")[1];
var seletorFeijoada = document.getElementsByTagName("select")[1];
seletorFeijoada.addEventListener('change',function(){

	var feijoada = {
		"Linguiça Defumada": 33,
		"Paio": 33,
		"Bacon": 20,
		"Carne-Seca": 83,
		"Lombo": 33,
		"Costela Defumada": 33,
		"Rabo": 17,
		"Orelha":17,
		"Feijão Preto":167
	}

	var stringFeijoada = "";

	var indice = seletorFeijoada.selectedIndex+1;

	for (item in feijoada){
		stringFeijoada = stringFeijoada + "<li>"+(indice*feijoada[item]).toFixed(0)+"g de "+item+"</li>"
	}
	listaFeijoada.innerHTML=stringFeijoada;

})

}