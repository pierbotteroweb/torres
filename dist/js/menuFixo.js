var navbar_torres = document.getElementsByClassName("navbar-torres")[0];
var navbar_social = document.getElementsByClassName("navbar-social")[0];
var carousel_torres = document.getElementsByClassName("carousel-torres")[0];
var w580 = window.matchMedia("(max-width:580px)");
var linkMenu = document.getElementsByClassName("linkMenu");

for (i=0;i<linkMenu.length;i++){
  let indice = i;
  linkMenu[indice].onclick = removeClasses;
}

function adicionaClasses(){
    navbar_torres.classList.add("navbar-torres-fixo","navbar-fixed-top");
    navbar_social.classList.add("navbar-fixed-top");
    carousel_torres.classList.add("carousel-torres-margin");  
}


function removeClasses(){
    navbar_torres.classList.remove("navbar-torres-fixo","navbar-fixed-top");
    navbar_social.classList.remove("navbar-fixed-top");
    carousel_torres.classList.remove("carousel-torres-margin"); 
}

if(w580.matches){ 
  adicionaClasses();
  onscroll = function(){
    if(scrollY<5){ 
      adicionaClasses();
      }
      else {
      removeClasses();
    }
  }
};
