
  'use strict';
  var $filtros = $('.filter [data-filter]'),
  $filtros2 = $('.filter2 [data-filter]'),
  $filtros3 = $('.filter3 [data-filter]'),
  $boxes = $('.boxes [data-tipo]'),
  $boxes2 = $('.boxes2 [data-tipo2]'),
  $boxes3 = $('.boxes3 [data-tipo3]');


  $filtros.on('click',function(a){
    a.preventDefault();
    var $this = $(this);

    $filtros.removeClass('ativo');
    $this.addClass('ativo');
 
    var $filtroTipo = $this.attr('data-filter');

    if($filtroTipo == 'all'){
      $boxes.removeClass('animacao')
      .fadeOut().promise().done(function(){
          $boxes.addClass('animacao').fadeIn();
        });
    } else {
      $boxes.removeClass('animacao')
      .fadeOut().promise().done(function(){
          $boxes.filter('[data-tipo = "'+$filtroTipo+'"]')
            .addClass('animacao').fadeIn();
        });
    }

  });


  $filtros2.on('click',function(a){
    a.preventDefault();
    var $this = $(this);

    $filtros2.removeClass('ativo-torres');
    $this.addClass('ativo-torres');
 
    var $filtroTipo2 = $this.attr('data-filter');

    if($filtroTipo2 == 'all2'){
      $boxes2.removeClass('animacao')
      .fadeOut().promise().done(function(){
          $boxes2.addClass('animacao').fadeIn();
        });
    } else {
      $boxes2.removeClass('animacao')
      .fadeOut().promise().done(function(){
          $boxes2.filter('[data-tipo2 = "'+$filtroTipo2+'"]')
            .addClass('animacao').fadeIn();
        });
    }

  });



  $filtros3.on('click',function(a){
    a.preventDefault();
    var $this = $(this);

    $filtros3.removeClass('ativo-vermelho');
    $this.addClass('ativo-vermelho');
 
    var $filtroTipo3 = $this.attr('data-filter');

    if($filtroTipo3 == 'all3'){
      $boxes3.removeClass('animacao')
      .fadeOut().promise().done(function(){
          $boxes3.addClass('animacao').fadeIn();
        });
    } else {
      $boxes3.removeClass('animacao')
      .fadeOut().promise().done(function(){
          $boxes3.filter('[data-tipo3 = "'+$filtroTipo3+'"]')
            .addClass('animacao').fadeIn();
        });
    }

  });





