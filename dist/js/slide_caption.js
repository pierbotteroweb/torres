$(document).ready(function(){
          $('.carousel-caption').hide();  
      $('.active .carousel-caption').delay(1500).addClass('slideInLeft').show();
      $('.carousel').on('slid.bs.carousel', function() {
         $('.carousel-caption').hide();
         $('.carousel-caption').removeClass('slideInLeft');
         $('.active .carousel-caption').offsetWidth = $('.active .carousel-caption').offsetWidth;
         $('.active .carousel-caption').delay(1500).addClass('slideInLeft').show();
      });
});