var gulp = require('gulp');
var browserSync = require('browser-sync').create();
var sass = require('gulp-sass');
var inject = require('gulp-inject');
var wiredep = require('wiredep').stream;
var del = require('del');
var mainBowerFiles = require('main-bower-files');
var filter = require('gulp-filter');
var concat = require('gulp-concat');


gulp.task('servico',function(){
	browserSync.init({
		server: "./dist"
	});
	gulp.watch("src/main.scss",['styles']);
});

gulp.task('clean',function(){
	del(['dist']);
});

gulp.task('styles',function(){
	var injectAppFiles = gulp.src('src/styles/*.scss',{read: false});
	var injectGlobalFiles = gulp.src('src/global/*.scss',{read:false});

	function transformFilepath(filepath){
		return '@import "' + filepath + '";';
	};

	var injectAppOptions = {
		transform: transformFilepath,
		starttag: '// inject:app',
		endtag: '// endinject',
		addRootSlash: false
	};

	var injectGlobalOptions = {
		transform: transformFilepath,
		starttag: '// inject:global',
		endtag: '// endinject',
		addRootSlash: false
	};


	return gulp.src('src/main.scss')
		.pipe(wiredep())
		.pipe(inject(injectGlobalFiles,injectGlobalOptions))
		.pipe(inject(injectAppFiles, injectAppOptions))
		.pipe(sass())
		.pipe(gulp.dest('dist/styles'))
		.pipe(browserSync.stream());
});

gulp.task('bsjavascript',function(){


	return gulp.src(['bower_components/bootstrap/dist/js/bootstrap.min.js','bower_components/jquery/dist/jquery.min.js'])
		.pipe(gulp.dest('dist/js'));
});

gulp.task('vendors',function(){
	return gulp.src(mainBowerFiles())
	.pipe(concat('vendors.css'))
	.pipe(gulp.dest('dist/styles'));
});

gulp.task('inject',['styles','vendors'],function(){
	var injectFiles = gulp.src(['dist/styles/vendors.css','dist/styles/main.css']);

	var injectOptions = {
		addRootSlash:false,
		ignorePath:['src','dist']
	};

	return gulp.src('src/*.html')
		.pipe(inject(injectFiles, injectOptions))
		.pipe(gulp.dest('dist'));

});

gulp.task('fonts',function(){
	return gulp.src('bower_components/bootstrap/fonts/*')
		.pipe(gulp.dest('dist/fonts/bootstrap/'));
});

gulp.task('montagem',function(){

	function montaPagina(x,final){
		return gulp.src(['src/modules/_header.html','src/modules/'+x,'src/modules/_footer.html'])
		.pipe(concat(final))
		.pipe(gulp.dest('src'))
	}

	montaPagina('_empresa.html','empresa.html');
	montaPagina('_index.html','index.html');
	montaPagina('_produtos.html','produtos.html');
	montaPagina('_dicas.html','dicas.html');
})



gulp.task('default',['montagem','servico','inject','bsjavascript','fonts'],function(){
	gulp.watch(['src/**/*.html'],['inject']).on('change',browserSync.reload);
	gulp.watch(['src/**/*.scss'],['styles']);
	gulp.watch(['src/modules/*.html'],['montagem'])
});
